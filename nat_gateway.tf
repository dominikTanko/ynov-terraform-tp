resource "azurerm_nat_gateway" "nat_gateway_dev" {
    
    name                = "nat-gateway-ynov-dev-1"
    resource_group_name = azurerm_resource_group.rg_dev.name
    location            = azurerm_resource_group.rg_dev.location

    tags = {
        environment = "dev"
    }
}