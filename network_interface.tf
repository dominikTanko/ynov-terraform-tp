resource "azurerm_network_interface" "nic_dev" {
        
    name                = "nic-ynov-dev-1"
    resource_group_name = azurerm_resource_group.rg_dev.name
    location            = azurerm_resource_group.rg_dev.location

    ip_configuration {
        name                          = "ipconfig-ynov-dev-1"
        subnet_id                     = azurerm_subnet.subnet_dev.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.pip_dev.id
    }

    tags = {
        environment = "dev"
    }

}