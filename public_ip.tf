resource "azurerm_public_ip" "pip_dev" {
    
    name                = "pip-ynov-dev-1"
    resource_group_name = azurerm_resource_group.rg_dev.name
    location            = azurerm_resource_group.rg_dev.location
    allocation_method   = "Dynamic"

    tags = {
        environment = "dev"
    }

}