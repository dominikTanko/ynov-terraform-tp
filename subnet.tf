resource "azurerm_subnet" "subnet_dev" {
        
    name                 = "subnet-ynov-dev-1"
    resource_group_name  = azurerm_resource_group.rg_dev.name
    virtual_network_name = azurerm_virtual_network.vnet_dev.name
    address_prefixes     = ["192.168.0.0/24"]
    
}