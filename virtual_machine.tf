resource "azurerm_linux_virtual_machine" "vm_dev" {

    name                = "vm-ynov-dev-1"
    resource_group_name = azurerm_resource_group.rg_dev.name
    location            = azurerm_resource_group.rg_dev.location
    size                = "Standard_B1s"
    admin_username      = "ynov"
    network_interface_ids = [azurerm_network_interface.nic_dev.id]
    admin_ssh_key {
        username       = "ynov"
        public_key     = file("~/.ssh/id_rsa.pub")
    }
    os_disk {
        name              = "vm-ynov-dev-1-os-disk"
        caching           = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }
    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }
    tags = {
        environment = "dev"
    }

}