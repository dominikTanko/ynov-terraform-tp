resource "azurerm_virtual_network" "vnet_dev" {

    name                = "vnet-ynov-dev-1"
    resource_group_name = azurerm_resource_group.rg_dev.name
    location            = azurerm_resource_group.rg_dev.location
    address_space = ["192.168.0.0/24"]

}